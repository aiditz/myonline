import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AudiosComponent }      from './audios/audios.component';
import { UploadComponent } from './upload/upload.component';

const routes: Routes = [
  { path: '', redirectTo: '/audios', pathMatch: 'full' },
  { path: 'audios', component: AudiosComponent },
  { path: 'upload', component: UploadComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}

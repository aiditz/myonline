import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import { ApiModule } from '../api/api.module';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})
export class UploadComponent implements OnInit {
  constructor(
    private http: HttpClient,
    private api: ApiModule
  ) { }

  myFiles: string [] = [];
  sMsg: string = '';

  ngOnInit () {  }

  getFileDetails (e) {
    //console.log (e.target.files);
    for (let i = 0; i < e.target.files.length; i++) {
      this.myFiles.push(e.target.files[i]);
    }
  }

  uploadFiles () {
    const url = this.api.buildUrl('audios/upload');

    for (let i = 0; i < this.myFiles.length; i++) {
      const frmData = new FormData();

      frmData.append('mp3', this.myFiles[i]);
      this.http.post(url, frmData).subscribe(
        data => {
          // SHOW A MESSAGE RECEIVED FROM THE WEB API.
          this.sMsg = data as string;
          console.log(this.sMsg);
        },
        (err: HttpErrorResponse) => {
          console.log(err.message);    // SHOW ERRORS IF ANY.
        }
      );
    }

  }
}

import { Injectable } from '@angular/core';
import { Audio } from './audio';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ApiModule } from '../api/api.module';

@Injectable()
export class AudioService {

  constructor(
    private http: HttpClient,
    private api: ApiModule
  ) { }

  getAudios(): Promise<Audio[]> {
    const url = this.api.buildUrl('/audios');
    return this.http.get<Audio[]>(url).toPromise();
  }

}

import { Component, OnInit } from '@angular/core';
import { Audio } from '../audio/audio';
import { AudioService } from '../audio/audio.service';

@Component({
  selector: 'app-audios',
  templateUrl: './audios.component.html',
  styleUrls: ['./audios.component.css']
})
export class AudiosComponent implements OnInit {
  audios: Audio[];
  selectedAudio: Audio;

  constructor(private audioService: AudioService) {

  }

  async ngOnInit() {
    await this.getAudios();
  }

  async getAudios(): Promise<void> {
    this.audios = await this.audioService.getAudios();
  }

  onSelect(audio: Audio): void {
    this.selectedAudio = audio;
  }
}

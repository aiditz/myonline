import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: []
})
export class ApiModule {
  private _baseUrl = 'http://localhost:4000/api/';
  private _token: string = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVhZGI3OTc3OGFjZWZlN2I5MTVlNWIzMiIsImlhdCI6MTUyNDMzNzIwM30.1hEmzJ9pDbRDVGLME0wMfeTaQyqmksDGG1Kh7kfJr3g';

  login() {

  }

  get token(): string {
    return this._token;
  }

  buildUrl(url) {
    let result = this._baseUrl;

    if (url.startsWith('/')) {
      url = url.slice(1);
    }

    result += url;

    if (!result.includes('?')) {
      result += '?';
    }

    result += 'token=' + this._token + '&';

    return result;
  }
}

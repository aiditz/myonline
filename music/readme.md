# Service to store and serve music

## Models

#### User

* `_id: ObjectId`
* `login: String`
* `password: String`

#### File

* `_id: ObjectId`
* `size: Number`
* `checksum: String`

#### Audio

* `_id: ObjectId`
* `owner: ObjectId<User>`
* `artist: String`
* `title: String`
* `fullTitle: String` - `artist + ' - ' + title`
* `file: ObjectId<File>`
* `lyrics: String`
* `playlists: Array<ObjectId>` - links to playlists
* `playedCount: Number`
* `bitrate: Number` - mp3 bitrate: 128, 256, 320, etc
* `id3Tags: Object` - raw id3 tags

#### Playlist

* `_id: ObjectId`
* `owner: ObjectId<User>`
* `name: String`

## API

The base URL for API calls: `http://host/api`.

To make API requests, you should provide a token via `token` query or body param or `X-Token` http header.
To get a token, make `POST /auth/login` request with login and password.

#### RESTful API

Some models has a set of endpoints to get, create, update and delete documents, implemented by [express-restify-mongoose](https://florianholzapfel.github.io/express-restify-mongoose/#usage-with-requesthttpswwwnpmjscompackagerequest) package.
Their prefixes:

* `/files/...`
* `/audios/...`
* `/playlists/...`

#### Other endpoints:

* `POST /auth/login` - get a token to make API requests.
    * `login`
    * `password`
    
* `POST /audios/upload` - upload a mp3 file. Only one of the fields `url` and `file` must be specified.
    * `url` - download the file from this url _(not implemented)_
    * `mp3` - file data in multipart/form-data request
    
* `POST /audios/:id/grab` _(not implemented)_ - copy a public audio to the current user's library 

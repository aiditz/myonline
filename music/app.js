const express = require('express');
const config = require('config');
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();

app.use(bodyParser.json());
app.use(cors());

app.use(require('./backend'));

app.listen(config.get('port'), () => {
  console.log(`Express server listening on port ${config.get('port')}`);
});

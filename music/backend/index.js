const express = require('express');
const mongoose = require('mongoose');
const config = require('config');
const restify = require('express-restify-mongoose');
const router = express.Router();

mongoose.connect(config.get('mongoose.url'));

restify.defaults(require('./common/restify.options'));

// auth by login/password - get token
router.use('/api/auth', require('./api/auth/index'));

// auth by token
router.use('/api', require('./api/auth/jwt.middleware'));

// api
router.use('/api', require('./api'));

// error handling
router.use('/api', require('./common/express.error.handler'));

module.exports = router;
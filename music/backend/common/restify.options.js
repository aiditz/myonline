module.exports = {
  version: '',
  allowRegex: false,
  findOneAndUpdate: false,
  findOneAndRemove: false,
  autoGenerateUrlPrefixes: false,

  access(req) {
    if (req.user) {
      return 'protected';
    } else {
      return 'public';
    }
  },

  contextFilter(model, req, done) {
    if (!model.schema.path('owner')) {
      return done(model.find());
    }

    done(model.find({
      owner: req.user._id
    }))
  },

  preCreate: (req, res, next) => {
    req.body.owner = req.user._id;

    next();
  },

  preUpdate: (req, res, next) => {
    if (!isOwner(req)) {
      return res.sendStatus(401)
    }

    next();
  },

  preDelete: (req, res, next) => {
    if (!isOwner(req)) {
      return res.sendStatus(401)
    }

    next();
  }
};

function isOwner(req) {
  const userId = req.user._id.toString();
  let ownerId = req.erm.document.owner;

  if (ownerId._id) {
    ownerId = ownerId._id;
  }

  ownerId = ownerId.toString();

  return ownerId === userId || req.erm.document._id.toString() === userId;
}
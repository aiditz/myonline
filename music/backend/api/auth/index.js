const express = require('express');
const router = express.Router();
const controller = require('./auth.contoller');

router.get('/login', controller.login);

module.exports = router;
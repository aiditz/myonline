const User = require('../user/user.model');

module.exports = async function(req, res, next) {
  try {
    const token = req.query.token || req.body.token || req.headers['X-Token'];

    if (!token) {
      throw new Error('Token required');
    }

    req.user = {
      _id: await User.verifyToken(token)
    };

    next();
  } catch (err) {
    next(err);
  }
};

const User = require('../user/user.model');

module.exports = {
  async login(req, res, next) {
    try {
      let login = req.query.login;
      const password = req.query.password;

      if (!login || !password) {
        throw new Error('Login and password required');
      }

      login = login.toLowerCase();

      const user = await User.findOne({login});

      if (!user) {
        throw new Error('User not found');
      }

      if (password !== user.password) {
        throw new Error('Wrong password');
      }

      res.json({token: user.getToken()}).end();
    } catch (err) {
      next(err);
    }
  }
};
const express = require('express');
const router = express.Router();

router.use('/files', require('./file'));
router.use('/audios', require('./audio'));
router.use('/playlists', require('./playlist'));

module.exports = router;
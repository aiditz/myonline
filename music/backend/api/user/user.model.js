const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');

const schema = mongoose.Schema({
  login: {
    type: String,
    required: true,
    unique: true,
    lowercase: true,
    access: 'protected'
  },
  password: {
    type: String,
    required: true,
    access: 'private'
  }
});

schema.methods.getToken = function() {
  return jwt.sign({id: this._id}, this.password);
};

schema.statics.verifyToken = async function(token) {
  let payload;

  try {
    payload = jwt.decode(token);
  } catch (err) {
    throw new Error('Token is invalid');
  }

  const user = await this.findById(payload.id, {password: 1});

  if (!user) {
    throw new Error('User not found');
  }

  try {
    jwt.verify(token, user.password)
  } catch (err) {
    throw new Error('Token not verified');
  }

  return payload.id;
};

const Model = mongoose.model('User', schema);

module.exports = Model;
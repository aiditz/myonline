const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.ObjectId;
const jschardet = require('jschardet');
const encoding = require('encoding');

const schema = mongoose.Schema({
  owner: {
    type: mongoose.Schema.ObjectId,
    ref: 'User',
    required: true,
    access: 'protected'
  },
  artist: {
    type: String,
    required: true,
    default: 'Unknown Artist'
  },
  title: {
    type: String,
    required: true,
    default: 'Unknown Title'
  },
  fullTitle: {
    type: String,
    required: true
  },
  file: {
    type: ObjectId,
    ref: 'File',
    required: true
  },
  bitrate: {
    type: Number,
    required: true
  },
  duration: {
    type: Number,
    required: true
  },
  lyrics: String,
  playlists: [{
    type: ObjectId,
    ref: 'playlist',
    access: 'protected'
  }],
  playedCount: {
    type: Number,
    default: 0,
    access: 'protected'
  }
});

schema.methods.readFromId3Tags = function(id3tags) {
  const enc = detectId3Encoding(id3tags);

  let temp = convertEncoding(id3tags.tags.artist, enc);
  if (temp) {
    this.artist = temp;
  }

  temp = convertEncoding(id3tags.tags.title, enc);
  if (temp) {
    this.title = temp;
  }
};

schema.methods.refreshFullTitle = function() {
  const arr = [];

  if (this.artist) {
    arr.push(this.artist);
  }

  if (this.title) {
    arr.push(this.title);
  }

  this.fullTitle = arr.join(' - ');
};

schema.pre('validate', function() {
  if (this.isModified('artist') || this.isModified('title')) {
    this.refreshFullTitle();
  }
});

function detectId3Encoding(id3tags) {
  const s = (id3tags.tags.title || '') + (id3tags.tags.artist || '') + (id3tags.tags.album|| '');

  return jschardet.detect(s).encoding;
}

function convertEncoding(data, enc) {
  return encoding.convert(data, 'utf-8', enc);
}

module.exports = mongoose.model('Audio', schema);
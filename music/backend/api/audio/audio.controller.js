const formidable = require('formidable');
const util = require('util');
const FileService = require('../file/file.service');
const AudioService = require('./audio.service');

module.exports = {
  async upload(req, res, next) {
    try {
      const form = new formidable.IncomingForm();

      const files = await new Promise((resolve, reject) => {
        form.parse(req, (err, fields, files) => {
          if (err) {
            return reject(err);
          }

          resolve(files);
        });
      });

      if (!files.mp3) {
        throw new Error('File content must be placed in a field with name \'mp3\'');
      }

      const file = await FileService.upload(files.mp3);
      const audio = await new AudioService(req.user).create(file);

      res.status(201).json(audio).end();
    } catch (err) {
      next(err);
    }
  }
};

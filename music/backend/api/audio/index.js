const express = require('express');
const restify = require('express-restify-mongoose');
const Model = require('./audio.model');
const controller = require('./audio.controller');
const router = express.Router();

router.post('/upload', controller.upload);

restify.serve(router, Model);

module.exports = router;
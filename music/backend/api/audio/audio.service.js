const mp3Info = require('mp3-duration');
const jsmediatags = require('jsmediatags');
const Audio = require('../audio/audio.model');

class AudioService {
  constructor(user) {
    this.user = user;
  }

  async create(file) {
    const audioData = {
      owner: this.user._id,
      file: file._id
    };

    // read id3 tags
    const id3tags = await new Promise((resolve, reject) => {
      jsmediatags.read(file.getLocalPath(), {
        onSuccess: resolve,
        onError: () => resolve()
      });
    });

    // read duration and bitrate
    await new Promise((resolve, reject) => {
      mp3Info(file.getLocalPath(), (err, {duration, bitRate}) => {
        if (err) {
          return reject(err);
        }

        audioData.duration = duration;
        audioData.bitrate = bitRate;
        resolve();
      });
    });

    const audio = new Audio(audioData);

    if (id3tags) {
      audio.readFromId3Tags(id3tags);
    }

    return await audio.save();
  }
}

module.exports = AudioService;
const express = require('express');
const restify = require('express-restify-mongoose');
const File = require('./file.model');
const router = express.Router();

restify.serve(router, File);

module.exports = router;
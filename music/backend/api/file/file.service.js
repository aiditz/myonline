const fs = require('fs-extra');
const config = require('config');
const mongoose = require('mongoose');
const checksum = require('checksum');
const File = require('./file.model');
const ObjectId = mongoose.Types.ObjectId;

const uploadDir = config.get('uploadDir');

class FileService {

  static async upload({size, name, path, type}) {
    if (type !== 'audio/mp3') {
      throw new Error('Only mp3 files supported');
    }

    const id = new ObjectId();
    const file = new File({_id: id});

    file.name = name;
    file.size = size;
    file.checksum = await calcCheckSum(path);

    const collisionFile = await File.findOne({checksum: file.checksum});

    if (collisionFile) {
      await fs.unlink(path);

      return collisionFile;
    }

    await fs.copy(path, file.getLocalPath());

    return await file.save();
  }

}

function calcCheckSum(filePath) {
  return new Promise((resolve, reject) => {
    checksum.file(filePath, function (err, sum) {
      if (err) {
        return reject(err);
      }

      resolve(sum);
    })
  });
}

module.exports = FileService;
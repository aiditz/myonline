const path = require('path');
const config = require('config');
const mongoose = require('mongoose');

const schema = mongoose.Schema({
  name: String,
  size: {
    type: Number,
    required: true
  },
  checksum: {
    type: String,
    required: true,
    index: true
  }
});

schema.methods.getLocalPath = function() {
  return path.resolve(config.get('uploadDir'), this.id + '.mp3');
};

schema.methods.getDownloadUrl = function() {
  return path.resolve(config.get('downloadUriPrefix'), this.id + '.mp3');
};

const File = mongoose.model('File', schema);

File.restifyOptions = {

};

module.exports = File;
const express = require('express');
const restify = require('express-restify-mongoose');
const Model = require('./playlist.model');
const router = express.Router();

restify.serve(router, Model);

module.exports = router;
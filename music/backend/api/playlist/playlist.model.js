const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.ObjectId;

const schema = mongoose.Schema({
  owner: {
    type: ObjectId,
    ref: 'User',
    required: true,
    access: 'protected'
  },
  name: {
    type: String,
    required: true,
    access: 'protected'
  }
});

module.exports = mongoose.model('Playlist', schema);